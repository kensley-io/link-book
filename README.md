# Link Book

Finding things on DndBeyond is hard. This is a fast reference page.   
Want to help? Put in a [Ticket](https://gitlab.com/kensley-io/link-book/-/issues)

### DnDBeyond Meta Filters
- [Spells](https://www.dndbeyond.com/spells)
- [Classes](https://www.dndbeyond.com/classes)
- [Races](https://www.dndbeyond.com/races)
- [Feats](https://www.dndbeyond.com/feats)

## Sources
| **PHB**                                                                                     | **DMG**                                                   |
|:--------------------------------------------------------------------------------------- |:----------------------------------------------------- |
| [Book Index](https://www.dndbeyond.com/sources/phb)                                     | [Book Index](https://www.dndbeyond.com/sources/dmg) |
| [Languages](https://www.dndbeyond.com/sources/phb/personality-and-background#Languages) |                                                       |
| [Armor and Shields](https://www.dndbeyond.com/sources/phb/equipment#ArmorandShields)    |                                                       |
| [Weapons](https://www.dndbeyond.com/sources/phb/equipment#Weapons)                      |                                                       |
| [Weapon Properties](https://www.dndbeyond.com/sources/phb/equipment#WeaponProperties)        |                                                       |
| [Adventuring Gear](https://www.dndbeyond.com/sources/phb/equipment#AdventuringGear)     |                                                       |

